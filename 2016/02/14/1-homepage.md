## Oliver P. Hoffmann

[![Foo](https://lh3.googleusercontent.com/-XWUR4aEpKxk/Vnfwys9GTYI/AAAAAAABCAg/7eY6LHj03VY/s400-Ic42/ontore.jpg)](https://at.linkedin.com/in/ontore)

---

## LinkedIn

[![LinkedIn](https://cdn4.iconfinder.com/data/icons/social-messaging-ui-color-shapes-2-free/128/social-linkedin-circle-128.png)](https://at.linkedin.com/in/ontore)

---

## Xing

[![Xing](http://www.baynado.de/blog/wp-content/uploads/2015/10/xing-logo-150x150.png)](https://www.xing.com/profile/OliverP_Hoffmann)

---

## Google+

[![Google+](https://cdn3.iconfinder.com/data/icons/ultimate-social/150/34_google_plus-128.png)](https://plus.google.com/+OliverHoffmannAustria)

---

## ResearchGate

[![ResearchGate](http://juhakoivisto.com/wp/wp-content/uploads/2014/11/researchgate.png)](https://www.researchgate.net/profile/Oliver_Hoffmann3)

---

## Academia

[![Academia](https://upload.wikimedia.org/wikipedia/commons/d/dd/Black-academia-logo.png)](https://independent.academia.edu/OliverHoffmann)
